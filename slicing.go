package slicing

func ArrayShift(a *[]interface{}) interface{} {
	if len(*a) == 0 {
		return nil
	}
	s := (*a)[0]
	*a = (*a)[1:]

	return s
}

func ArrayPop(a *[]interface{}) interface{} {
	if len(*a) == 0 {
		return nil
	}
	last := len(*a) - 1
	s := (*a)[last]
	*a = (*a)[:last-1]

	return s
}
